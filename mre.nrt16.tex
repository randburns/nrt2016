
While IDIES has been a leading institution defining the course of eScience and big data,
exascale computing mandates that we pivot toward research that dynamically and 
interactively links computing and analysis.  Our last decade of research has leveraged
massive storage, Web-services, and cloud computing to democratize access to 
world-class scientific data.  This started with the Sloan Digital Sky Survey, which
has created a worldwide virtual astronomy community--more that 6000 refereed papers have 
used its data and there are 7000 registered users for a professional astronomy community 
of only 15,000 people. We also introduced the notion of {\em immersive} analysis of simulations
in which we capture the entire space-time history of an experiment for post-hoc analysis
over the Internet.  Eyink et al.~\cite{Eyink13} exploited a database search to show that a 
70-year-old belief about high-conductivity plasmas---magnetic flux freezing---fails in the 
presence of MHD turbulence, explaining why solar flares can erupt in minutes or hours 
rather than the millions of years predicted by flux freezing.

However, the model of capturing datasets for post-hoc analysis breaks down for exascale.
The simulation state is too large to be transferred over networks or stored on file systems
\cite{Szalay06,Bell09}.
Going forward all useful analysis must
be performed in-situ, on the memory of compute nodes while the simulation is running, or
in transit as data moves across networks and through temporary storage, such as burst buffers \cite{Liu12}.
Similarly, simulations will use tens of millions of hours of processor time and must be
carefully directed by models, steered by visualization, or driven by analysis.

The way forward lies in creating two-way connections between in-situ analysis and databases.
In-situ analysis will automatically generate a database sufficient to
verify and repeat analysis and generate provenance that connects analysis to simulation.
In the other direction, analysis queries to the database will launch simulation that improves or expands
the database to explore a new science question.  The remainder of this section describes
emerging research projects at IDIES that link computation and analysis to which trainees
will affiliate.  These include analysis extensions to numerical simulations and novel 
methods for data-driven simulation, using analysis to drive computation.

We highlight IDIES emerging research, including data-science projects in four numerical simulations 
and two techniques for connecting supercomputing and analytics.  These projects have emerged from 
our existing funding.  The NRT will accelerate these projects by creating the 
$\Pi$-shaped trainees capable of making connections across a discipline and data science.

%\rb{CFP: : Describe the novel, potentially transformative research that the NRT will catalyze through interdisciplinary synergies emerging from currently funded activities at the institution(s) and/or via separate NRT-funded interdisciplinary initiatives. Explain the need for the proposed NRT research and how it would substantially advance, inform, and transform research beyond funded initiatives already underway at the institution(s). NRT funding should be used to complement rather than supplant other research funding.}


\input{turb.nrt16}
\input{cosmo.nrt16}

\para{Yield and Fracture of Materials} 
Rare events triggered in local regions with extremal properties often dominate these phenomena.
Examples include nucleation and advance of cracks,  intermittent ``avalanches'' of activity in granular flow, and 
solid deformation at scales 
from nanometers to the earth's crust \cite{dahmen,SalernoRobbins}. Major outstanding theoretical challenges 
are to predict where deformation will initiate, 
how it will evolve, and how it will affect the subsequent structure and response of a system (Figure \ref{materials}). 
Advancing the predictive capabilities for strength of materials is 
an important part of the {\it Materials Genome Initiative} \cite{mgi}.

\begin{wrapfigure}{r}{0.55\textwidth}
	      \vskip -0.10in
              \includegraphics[width=0.55\textwidth]{figs/fig-materials.pdf}
              \vskip -0.15in
\caption{\small Left: pattern of shear deformation after 0.1\% strain. 
Right: region deformed in one avalanche (blue), initiation site (magenta) 
and possible initiations sites  (black and green).}
\vskip -0.15in
\label{materials}
\end{wrapfigure}
Capturing the onset and spatio-temporal evolution of rare events poses significant computational challenges.
During short intervals, events must be resolved on the finest time scale used to integrate the equations of motion. %and changes propagate at the speed of sound.
However, these events are often nucleated long before the period of rapid change and may go 
through long quiescent periods before reactivating.
This makes it very difficult to detect when events begin and end until the system has evolved past the point of interest.
Rewinding the simulation is often impossible because round off errors lead to completely altered dynamics.
On the fly data analytics and a record of recent states will be combined to identify nucleation and preserve data about the initial state and evolution of the system.
This will allow resimulation to determine how sensitive the nucleation is to thermal activation or statistical fluctuations in materials properties.
On the fly and subsequent analysis will also be used to develop new methods for identifying the sites of incipient failure.
Current state of the art can only identify the top few percent of the system where failure is more likely and does not make quantitative predictions about the distance to failure \cite{Manning,Patinet16}.


\subsubsection{Numerical Techniques Linking Computation and Analysis}
\label{sec:resim}

The success of exascale computing will depend on meaningful ways to derive 
further computation from the analysis of an existing simulation.  In this way,
simulations can be run at lower resolutions, for fewer parameterizations, 
or partially captured to minimize storage.  Simulations may be enhanced at a later 
time to answer a specific science question or multiple outputs from disjoint 
simulations merged and compared. 
We present techniques that link computation and analysis for a broad class of applications.

\para{Resimulation}
Even globally resolved simulation may lack sufficient space-time resolution for extreme
events that, while very rare, are of intense interest. A good example is the possible Leray singularity of incompressible 
Navier-Stokes equation, the subject of the Clay Millennium 
Prize \cite{Fefferman_2000}. Such singularities have never been observed in 
simulation or experiment, even though they have remarkable distinguishing characteristics: e.g. the fluid velocity 
must diverge with distance $r$ to the singularity at least as $r^{-1}$ \cite{Cafarellietal_1982}. 
Suppose a point was observed in a conventional 
Navier-Stokes simulation that exhibited such a blow-up for $r$ down to the space resolution $\Delta x$ of the simulation.
This event would become a focus of world-wide scientific interest, but conventional simulation could shed no further light on 
the problem.  When running the same simulation with the same initial and boundary conditions with reduced $\Delta x$, 
the event of interest might no longer occur! This is due to the chaotic dynamics of high-Reynolds-number fluid dynamics. 
%If the putative Leray singularity is an occasional consequence of unstable dynamics, then round-off errors in the calculation might lead 
%to a new solution that would miss it entirely and produce a smoother behavior.  
This same difficulty arises for other questions of great interest, such as the most intense vorticity
\begin{wrapfigure}{r}{0.4\textwidth}
	      \vskip -0.05in
              \includegraphics[width=0.4\textwidth]{figs/fig-vortices.pdf}
              \vskip -0.15in
\caption{\small Vortices in turbulence and subregion with multiscale vortex interactions for resimulation (viz. by Dr. Kai Buerger, TUM).}
\vskip -0.1in
\label{vortices}
\end{wrapfigure}
events or the smallest length-scales possible in a turbulent flow \cite{Schumacher_2007}. 
Any event which is produced by a ``perfect storm'' of conditions would be impossible to study at 
finer resolutions by conventional methods.

Resimulation could provide such capability. The theoretical basis is the phenomenon of ``synchronization 
of chaos'', which allows one to recover non-chaotic features in the far-dissipation range---to whatever desired resolution---given a 
standard numerical Navier-Stokes solution resolved down to about the Kolmogorov scale \cite{Lalescuetal_2013}. A new simulation 
local in space and time around the feature of interest (Figure \ref{vortices}) can be performed that
 gives a more accurate Navier-Stokes solution at finer resolution, but which is completely consistent with the chaotic solution archived at lower resolution. 
%Combined 
%with the virtual sensor and visualization capabilities of the immersive laboratory, 
This resimulation capability will provide ``on-demand''  
refinement of the archived data. Particular localized features encountered in the data can be recomputed with a resolution much 
finer than that of the original simulation, providing a numerical ``zoom-in''  on events of special interest.  

\para{Multiscale modeling}
The evolution of solid and fluid systems is often controlled by processes on a wide range of scales.
In materials, atomic interactions determine the local constitutive response, but the macroscopic response is controlled by larger scale spatial structures including interfaces, dislocations, and grain boundaries.
%and the connectivity of long polymer chains.
Near-wall turbulence dynamics at very fine scales can drastically affect overall drag forces. And, ocean dynamics depend upon physical processes occurring at scales
 much smaller than those explicitly resolved in the numerical models.  The traditional approach has been to
\begin{wrapfigure}{l}{0.4\textwidth}
	      \vskip -0.07in
              \includegraphics[width=0.4\textwidth]{figs/fig-multiscale.jpg}
              \vskip -0.15in
\caption{\small Multiscale modeling of deformations resulting from contact between rough surfaces.  The model represents
volumes of about $10^{12}$ atoms with linear dimensions of several micrometers while retaining atomic resolution at 
the finest resolution.}
\vskip -0.2in
\label{fig:multiscale}
\end{wrapfigure}
approximate small scale behavior by simple analytic 
constitutive laws that often fail to capture the nonlinear, nonequilibrium response of real systems.  
In contrast, multiscale approaches 
 produce effective coarse-grained models that are informed by smaller fine-grained simulations \cite{E2007,Kevrekidis}. 
 These may be particle-based models or continuum models with fewer degrees of freedom. 
 The coarse-grained models are normally fit once, using a restricted data set. 
The concomitant accuracy of interpolation and extrapolation is unknown.
An alternative is to perform concurrent fine-grained simulations for the current state in regions of a coarse-grained system (Figure \ref{fig:multiscale}).
The concern is that this may sample phase space inefficiently and long simulations would be required to obtain accurate stresses and rates.

A data-driven supercomputing framework will enable us to approach the challenges of multiscale modeling in a new direction. We propose to develop DDMA, a Data-Driven Multiscale Approach.  DDMA will build an interactive  database based on results from past or on-the-fly fine-grained simulations. Such a growing database is searched automatically during coarse-grained simulations to identify relevant conditions and concomitant fine-grained parameters. In turn, the coarse-grained simulations identify the most important regions for fine-grained simulations that will be used by DDMA to improve the database. The particulars of this approach will be highly problem specific, i.e.~DDMA applied to coupling molecular dynamics to continuum mechanics will differ significantly from those in an ocean model coupling to small-scale dynamics or near-wall turbulence. However, common challenges, such as determining best structure of data to be transferred between simulation levels and the database organization and indexing for the required tight coupling with simulations, will be approached with a common set of criteria.
