
Our data management plan covers several distinct classes of data: (1) data collected as part of this
NRT proposal used for reporting, assessment, auditing, and performance evaluation; (2) 
course materials (syllabi, video lectures, course projects) released under and open source license
and, (3) the data sets produced by numerical simulations resulting from our major research efforts. 
All data sets will be archival; they are intended for long-term preservation and
subject to all of IDIES' data management policies described below.

\subsubsection*{Assessment and Reporting Data}

All of the data collected within the Performance Assessment/Project Evaluation as described in  
Section \ref{sec:eval} will be collected and stored in a relational database within IDIES. 

More importantly, we will use IDIES' extensive logging infrastructure to  track how students 
use data-intensive Web-services and supercomputing allocations for coursework and research.   
One of the most useful byproducts of the Sloan Digital Sky Survey  data has been the 
usage logs that we have kept since
the very beginning of the project: every Web hit and every single query have been logged since Day 1.
Today, the log database is over 2TB, and contains rich historical information about how astronomers
learned to access a virtual telescope \cite{Singh06}. 
This has resulted in an amazingly rich and useful resource
for SDSS scientists and project managers and, because the dataset is available to anyone, many other
projects and researchers. IDIES has generalized this logging infrastructure for all projects.

Based on our logging infrastructure, we will be able to analyze the data usage patterns of NRT trainees
and data-science students and use statistics to provide detailed feedback on 
course performance to our evaluator.  The goal is to inform the evaluation process 
with data-driven metrics at an arbitrarily fine granularity.  These data will be linked 
to the more traditional evaluation statistics, i.e. the databases will be co-registered 
for correlation studies.  All databases will be preserved for post-hoc analyses.

\subsubsection*{Licenses}
All scientific data sets will be released under the {\em Open Data Commons Attribution} (ODC-By) license.
The license allows for free use of all data products, including copying, sharing, and redistributing data, and
deriving new, customized data products.  Our specific license terms require that all subsequent
data use attributes the original publication that contributed the data as well as the project 
that hosts the data.  
All software and course material will be released under the Apache 2.0 license. This license is 
permissive; it places few restrictions on the use of software.
Derived software products or software systems that integrate our code or course materials 
do not need to be open-source.


\subsubsection*{Data Management at IDIES}

The education plan and major research efforts will leverage many 
publicly-accessible data sets and Web-services deployed on the 
Data-Scope cluster at the Johns Hopkins University Institute for Data-Intensive Science 
and Engineering (IDIES; \url{idies.jhu.edu}).
IDIES has successfully deployed data-intensive Web services that contribute 
computation and analysis to the public for several disciplines, including astronomy (sdss.org), 
computational fluids (\url{turbulence.pha.jhu.edu}), and neuroscience 
(\url{neurodata.io}). IDIES hosts more than 11 petabytes of contributed data in support of 
many disciplines.  IDIES has 13 years of experience 
hosting publicly accessible scientific databases and data-intensive Web-services. The best 
practices of the Institute will be adopted, as described below.

\para{Documentation and Metadata} Source code documentation will be collocated and released with 
software, governed by the same open source license, and hosted on public repositories. 
Documentation for datasets are treated similarly. They 
will be managed in version control public repositories. They will be available as links that are 
collocated with the Web-services that provide access to data. These are already standard practices 
of the collaborators. 

\para{Data Integrity} Our systems make several efforts to ensure the integrity of the
 data and to prevent malicious or unintended modification. We use RESTful interfaces 
 for data access and to execute analysis routines, ensuring that all interfaces are functional and 
 do not modify the original data. We also regularly crawl our data checking content against 
 pre-computed checksums to protect against corruption from hardware and software errors. 

\para{Security, Privacy, and Embargo}
  The privacy of data sets is implemented with a combination of a user authentication in 
  our project management system and the secure Web-services (\url{https://}).  
  The combination allows users to self-manage access control to their data.  They may 
  set individual data sets, images, and annotation projects as public or private.
  Derived data products may be kept private by registered users of the system. 
  Private data sets will be encrypted end-to-end for all Web-services and visualization tools.

  The contributors of data maintain control of the data even after placing it in the system.
  They can determine when to make data public, when to release data from embargo, etc.

\para{Storage and Backup During the Project} Selected datasets will be backed up on site and archived 
off-site. IDIES provides this capability to data tenants of the Data-Scope cluster (see 
“Facilities”). IDIES also participates in a a mirroring relationships with the University of Illinois at 
Chicago through the Open Cloud Consortium, using a dedicated 100 Gbps link to Internet 2.

\para{Long-Term Archival and Preservation} IDIES has preserved all scientific data that it has 
  ingested and will continue to do so in perpetuity. The first resources developed were the Sloan 
  Digital Sky Survey (sdss.org). Multiple strategies have been used to maintain these data. We 
  have partnered with Google to store all image data on an ongoing basis. The development of 
  new data-intensive clusters (GrayWulf 2006 and Data-Scope 2012) has provisioned a small 
  fraction of resources to maintain the entirety of our previously collected data. Our preservation 
  ethic includes preserving the function and semantics of the data long after project operation 
  completes. We will do so by defining archival packages that include algorithms and methods as 
  well as data. We recognize sustainable preservation as one of the most challenging aspects of 
  developing data resources and note our commitment and experience in defining strategies to 
  fund and maintain resource-sharing beyond project lifetimes. 

\para{Data Sharing and Dissemination} IDIES focuses on making public datasets broadly 
available, leveraging the team’s expertise in large-scale storage architectures and data 
processing to host scientific datasets that are unmanageable by the scientists that generate and
analyze the data. This process makes data a community resource, greatly enhancing their 
utility. All public datasets referenced in this project will be available through Web-services. We 
will publicize our datasets through outreach activities conducted by IDIES, which include 
museum exhibitions, the National Science Fair, and community programs.

\para{Ownership, Copyright, Intellectual Property} Our projects encourages scientists and communities 
that ``open source'' their data in exchange for storage and analysis services. Open-source data 
becomes a community resource, unrestricted for non-commercial use. Data providers retain 
copyright privileges and reserve licensing and approval rights for commercial uses of data. 
Users of the data reserve rights to their algorithms and analysis techniques.

