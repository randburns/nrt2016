

The goal of this NRT-DESE is to train a workforce for the National Strategic Computing Initiative 
(NSCI, \cite{NSCI}), creating developers of numerical simulations that are native in data-science 
in addition to their chosen field, such as physics, material science, mechanical engineering, 
or climate science. This goal aligns directly with the objective in the NSCI that targets
the software and applications gaps:  {\em ``Increasing coherence between the technology base used for 
modeling and simulation and that used for data analytic computing.''}  
To highlight the central role of this goal with respect to the NSCI, we note that the four other goals
address hardware (delivery of exascale and post-Moore's law processors) and 
community (HPC ecosystem and public-private partnerships).  Funded trainees will be PhD students
that perform exascale science, numerical simulations, visualizations, and analyses on exascale machines.
Additionally, the curriculum will be accessible to Master's students and undergraduates and
to students from our partner organizations Morgan State, an historically-black university, and 
the University of Maryland.

The NSCI lays out exascale computing as a path forward for scientific discovery and economic growth,
but realizing the potential of exascale will require fundamental shifts in the way that software
and data analytics are developed and deployed.  A numerical simulation at exascale becomes so
massive that the scientist can observe only the smallest fraction of the system's state;
it will produce 200-300 PB/s \cite{Sodani11} 
and only 1 TB/s \cite{Hick11} of that system 
will make it to persistent storage, i.e.~the scientists get to look at one byte in $10^6$. 
This renders useless the current practice of extracting the space-time history of a simulation 
for post-hoc visualization and analysis.  Rather, future applications will need to embed analysis
and visualization into the running simulation, performing {\em in-situ} analysis \cite{Woodring11}
on the contents of memory and {\em in-transit} analysis \cite{Bennett12} as data moves through networks and 
the storage hierarchy.  
In-situ and in-transit analysis dictate that the scientists 
must have a deep scientific understanding of the simulated system and be expert in 
data science, armed with algorithms, statistics, parallel programming, data wrangling, and 
visualization skills.  

Today our higher education process trains people who are ``I''-shaped---they become increasingly 
narrow and deep in a high specialized area of science as their scientific training progresses.
Later, some of these people become ``T''-shaped, mostly through extended collaborations with a 
broader science team. They acquire a broad, but much shallower knowledge, reaching across 
other scientific disciplines. Today, when so much of science revolves around data, the most 
successful people are becoming ``$\Pi$''-shaped, as in the Greek letter Pi. This symbolizes that,
besides the deep knowledge in their original discipline, scientists must become ``native'' in 
data science, the discipline of turning raw data into discoveries. They also 
need broad, interdisciplinary knowledge to appreciate the subtle similarities and differences 
of each domain.

Developing $\Pi$-shaped scientists requires a deep cultural shift in the supercomputing world.
Currently, each of these skills are held by different researchers in different departments. 
It is typical in national labs that the statisticians performing uncertainty quantification
have only a superficial understanding of the physics encoded in the data.  
%Anecdotally, PI Burns, as a visiting computer scientist on sabbatical at Los Alamos National Lab, has worked on 
%analytics and runtime systems for four applications and has met the development team for only one
%of these codes.  
Such scientific stovepipes are deeply entrenched, encoded culturally in academic 
communities and institutionally in departments. The current organization of academia, government, 
and industry will become increasingly dysfunctional as exascale computing demands a holistic
understanding of simulation, the scientific domain, statistical analysis, and computer science.

%This NRT program will train the next generation of scientists and engineers in 
%data-intensive scientific discovery. The effects of abundant data in complex science 
%and engineering domains are profound: modeling couples more tightly with empirical evidence,
% algorithms and analytics outperform naive, uninformed models and accelerate discovery 
%and understanding, and the data acts as a communication medium through which knowledge 
%can be disseminated and assessed. Consequently, science and engineering students in all 
%disciplines must become data scientists in addition to domain experts, 
%armed with data exploration, analysis, modeling, wrangling, and visualization skills to pursue a 
%research agenda with real-world impact.


Today, $\Pi$-shaped people largely happen by accident. The goal of our proposal 
is to establish a program in which such scientists come out of a well-planned 
and executed training process. This is quite a challenge; the organization of the program 
must be cut-across the deep, narrow channels of the current science curricula. 
We aim to establish a $\Pi$-hD program, a PhD with an interdisciplinary twist, in which each 
student comes out of the program ``native'' in both a chosen discipline and in data science.
We hold that graduate education is the only opportunity to gain truly interdisciplinary training.
Once students graduate, they enter a more competitive environment in which 
they can only learn data science ``on the job.''  They will never again have the time to 
gain a deep enough understanding to really become $\Pi$-shaped.  

%The PIs of this proposal
%have struggled with these issues in their career, we are largely $T$-shaped and 
%often struggle to do groundbreaking cross-cutting work with only a superficial understanding of
%our collaborator's fields.  We view this NRT as a unique opportunity to define a new type
%of curricula and create students more capable of meeting the exascale challenge than we are.

As current graduate curricula are packed with classes, one cannot simply add 
classes in computing and statistics as mandatory requirements.  
These amount to another graduate degree.  Nor can we afford to create a separate
curriculum for each field: physics, biology, etc.  Instead, 
we need to instill ``computational thinking'' in which  
all students learn to formulate a research problem so that the analytics remains solvable,
irrespective of the complexities of their data.

To this end, we have devised a modular course architecture 
that eliminates redundancy among classes
and allows students to construct the data science skills needed for their specific
research from a menu of one unit topic courses.  
We expect PhD students to combine nine modules and Masters students 
to combine six: a reasonable non-departmental load equivalent to three courses
for PhDs and two for Masters.

We will create a program that takes students from a wide range of science and 
engineering disciplines, and adds a ``second-leg'' in data science to their training,
 plus gives them more than the usual exposure across a broad range of science through
interdisciplinary teams and mentoring.
%This will result in graduating students who have a much broader training than before.
%Not every student will want to become fully $\Pi$-shaped and students may choose
%a smaller subset of topics relevant to their research. 
%The training structure will respect that. We will create a pyramidal structure, 
The curricula consist of highly compressed introductory classes designed for first-year 
graduate students and advanced undergraduates in 
the sciences and engineering (and medical and health sciences).
After, a customizable set of specialized and deeper classes 
%At the next level,  students choose from among a set of deeper classes
%teaching students more advanced techniques 
in data analytics and computing, the student 
will be able to solve small-scale, data-intensive problems by themselves.
Students will then pursue active research problems at the interface between data-intensive science
and domain science, co-advised by a faculty member in applied math or computer science and 
a faculty member studying numerical simulations in another science or engineering discipline.

The NRT program at JHU will build a much broader community on campus and provide a deep 
foundation in data science not only for those graduate students who are directly funded by the program,
but for all who participate in the introductory or advanced classes, including undergraduate and graduate
students from Morgan State. 
The domain areas will include a wide variety of topics, without diluting the program, 
as the data science components are domain-neutral.
% students from any discipline in science and engineering can attend those classes.
The specific areas in which core faculty will carry out cutting-edge research
derive from existing projects in numerical simulation and analysis in  
turbulence, astrophysics, materials science, and climate science.

We aim {\em to fundamentally and persistently change how the data sciences are taught} to 
scientists and engineers.  Through this NRT, we will develop and refine a novel data-science
program at 
Johns Hopkins that will serve generations of students to come.  We will also disseminate
video lectures, projects, and syllabi as open-source.  The distribution of course material  
will use a GitHub-like framework in which other instructors can fork content, customize it for 
their courses, and push it back.  Community contributions will be moderated, evaluated, and selectively 
merged back into repository.

%\tz{In the CFP: Address implications of the proposed NRT project for broadening participation.}
%\tz{Morgan UMD participation}.

We view the NRT as an opportunity to improve the participation of under-represented 
groups in fields related to data-science.  
To this end, our efforts must extend beyond recruitment. 
We have partnered with Morgan State University, an historically black university.  
Undergraduate and graduate students from Morgan State will be able to participate 
fully in the NRT modules offered at JHU and the courses will count towards their degrees.  
They will have access to the NRT curricula, infrastructure, and activities.  Their participation 
will also feedback to Morgan State and will grow the data-sciences community across 
institutional boundaries.  We have structured this collaboration so that students receive credit
at Morgan State for courses taken at Johns Hopkins.  
This arrangement allows Morgan State students to participate based on their existing enrollment, avoiding 
registration overheads and financial obligations.

To further expand the community, we have partnered with 
University of Maryland (UMD), College Park.  JHU and UMD share a high-performance computing 
facility with more than 900 TFLOPs peak performance (Maryland Advanced Research Computing Center, or MARCC). 
By making the NRT curricula available to their students, we leverage our shared resource to grow the 
data-science community.  

NRT graduates will be uniquely qualified to enter the high-performance
computing workforce, ready to bridge the knowledge gap between a science application
and exascale computing.  Outcomes will include placing trainees in national supercomputing
centers, such as those run by the NSF, DOD, DOE, NIH, and NSA, in 
industry with IDIES partners such as Microsoft, Intel, Nvidia, and Cray, and in 
DOD research centers, such as MIT Lincoln Labs and the JHU Applied Physics Laboratory.
We will support these outcomes by connecting students with summer internships, 
particularly in the DOE National Labs.  While such internships will not funded by 
this grant, there are formal summer programs at most sites and a critical need for
scientists with data-science skills.  Identifying internships will be a formal part of mentoring
and building long-term relationships with summer programs will be included in assessment.

