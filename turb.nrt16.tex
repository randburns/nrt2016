
\para{Hydrodynamic Turbulence} 
Understanding, modeling and controlling turbulent flows is a notoriously difficult challenge that led R. Feynman to remark that ``fluid turbulence remains the last most important unsolved problem in classical physics''.   In simulation-based turbulence research,  the underlying Navier-Stokes  equations are solved numerically, but progress has been hampered due to the immense amount of data and complexity of the solutions obtained.  The development of a publicly accessible, Web-Services equipped database tool (JHTDB, \cite{Li08,Eyink13}) has been instrumental in beginning to address these challenges. 

Moving forward, we aim to develop new tools to improve the prediction of turbulent flows near solid surfaces, including {\it transition from laminar to turbulent boundary layer flows}.         
The chaotic nature of turbulence is manifest from its inception: Rare events in a seemingly laminar flow trigger the non-linear process of breakdown to turbulence \cite{Hack_JFM_2014} (Figure \ref{turbulence}(a)).  High-fidelity simulations \cite{Zaki_FTC_2013} require very high spatial resolutions and must span very long times for meaningful analysis of rare events. Most crippling is the inability to reproduce these events from earlier simulations due to high sensitivity to the initial state. 

\begin{figure}[t]
\vskip -0.1in 
	\begin{center}
		\includegraphics[width=6.5in]{figs/fig-expeditions-turbulence.pdf}
	\end{center}
	\vskip -0.3in 
	\caption{\small (a) Snapshots from simulations of the onset and spreading of turbulence in boundary-layer flow. (b) Near-wall turbulent dynamics obtained from channel flow database for data-assisted wall model LES.} 
\label{turbulence}
\vspace{-5pt}
\end{figure}

Decisive progress in these problems is possible using data-driven supercomputing. On-the-fly analytics that identify the onset of turbulence are required in order to capture these rare events \cite{Nolan_JFM_2013}. Simulations can be dynamically traced back earlier in time, prior to turbulence inception in order to further refine the evolution of the flow in state space. The subsequent dynamics, in particular the ensuing flow structures, can be tracked in space-time as they develop, and the state can be fed back to a database for archiving. External users can access these rare initial states and the refined trajectories for analysis and resimulation.  

Another goal will be to develop {\it data-driven wall modeling for Large Eddy Simulations (LES)}. Traditional approaches have inherent limitations, making wall modeling for LES a pacing item for research \cite{Piomelli}. During LES, when the wall stress is needed at any given surface location, we can identify the local prevailing conditions in the LES (e.g., local pressure gradient) and search such conditions within a fully resolved DNS that is either computed concurrently  or archived in a suitably constructed database (Figure \ref{turbulence}(b)). The  local drag can then be rescaled   and fed back into the LES in simulation time. This approach could solve the long-standing problem of wall modeling in LES and enable more accurate predictions of drag forces. 
 

\para{Simulating Ocean Circulation}  

State-of-the-art ocean simulations are now approaching horizontal grid spacings of O(1)km that cover the globe 
(Figure \ref{fig:ocean}). Regional simulations at that resolution accurately resemble 
the sparse observations from ships, autonomous instruments, and satellites (in a statistical sense).
This milestone, separating the era when simulations were so coarse that they were obviously wrong, 
to the era when they are so refined that they cannot be distinguished from data,
 marks the coming-of-age of Computational Oceanography. This milestone is being passed in the present decade.

\begin{wrapfigure}{r}{0.6\textwidth}
\vspace{-10pt}
\includegraphics[width=0.6\textwidth]{figs/fig-ocean.pdf}

\vspace{-10pt}

\caption{\small Sea surface temperature from a global run of the MIT general circulation model with O(1)km horizontal resolution. Courtesy Chris Hill, MIT.}
\label{fig:ocean}
\vskip -0.10in
\end{wrapfigure}

The time is ripe to focus attention on open, efficient analysis of massive ocean circulation solutions.
 In this project, we will design and build protocols and infrastructure to provide access to benchmark 
global ocean circulation solutions at O(1)km resolution, for example, for a ``normal year'' of simulation.
We will construct software toolsets to operate on these data, such as Lagrangian diagnostics 
(simulating passive particles moving forwards and backwards in the flow [12]), computation of energy, 
vorticity, and potential vorticity budgets, and visualization for immersive exploration and final rendering.
Long-standing scientific questions that will be addressed this way include: Where, and when,
 does the ocean interior mix and transform water properties? 
Where should we make measurements to observe this highly intermittent process? 
What is the role of geostrophic eddies, on scales of O(10)km, in the low-frequency general 
circulation on scales of O(1000)km [13]? How does the ocean transport of mass, heat,
 and freshwater carbon depend on scale, and how sensitive are these fluxes to atmospheric 
forcing anomalies associated with climate change?

