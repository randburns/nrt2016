%%%% -*- mode: Latex; TeX-master: "nrt14.tex"; -*-
% Big topic ideas

Several central themes drive the design of our curricula and training. 
First, data science skills in computing and applied math are inaccessible
to students in other science and engineering disciplines.  While many of the topics
may be taught currently, they are embedded in curricula designed for CS and applied
math degrees that contains lots of redundancy and topics irrelevant to the data scientist.
Acquiring this knowledge in current curricula requires an entire additional graduate degree.
Our approach is to build a modular data science 
program for $\Pi$-shaped graduate students using the principles of 
``flipped'' courses \cite{Hall15}, domain-themed projects, and customization based on the 
learning needs of each student. 

Second, $\Pi$-shaped people result from interdisciplinary 
communities that persist throughout the student's graduate career.  This includes
a deep, small team focused on a specific research area and a broad
network that encompasses multiple disciplines to help make connections
and transfer knowledge.  The structure of our NRT creates communities through
a combination of: (1) formal co-advising in two (or more) departments, (2) a program-wide
colloquium, and (3) required oral and written communication courses to help students interact
effectively outside their discipline and across achievement and skill levels.

We expect that trainees will receive degrees in their chosen science or engineering discipline 
and gain proficiency in data science.  The program does not preclude students from a pursuing graduate 
degree in applied math or computer science; the courses will count toward these degrees.
However, that is not the goal.  The goal is to equip scientists with data-science skills
based on a modular curriculum that does not delay their graduation or research.

This proposal will support a total of 14 trainees for two years.  
Three or four students will be admitted in each of the first four years; we term this group
of three or four a cohort. 
Half of the trainees will be drawn from from underrepresented groups.
One trainee will be dedicated to each of the four major research efforts,
cosmology, turbulence, material fracture, and climate.
Two trainees will be assigned to computer science and two to applied math.
The remaining six slots will be assigned via an open competition that identifies the 
best pairing of candidates and interdisciplinary advising pairs from JHU's data-intensive 
science community.

We made the choice to provide trainees two full years of support to help achieve
our goal of workforce development.  Some students in their first PhD year will not 
be ready to enter the exascale workforce and may spend their first summer within a major 
research effort team at JHU. Other students may be prepared and conduct two different
internships, preferably one in a national lab and one in industry.  We are required
by the NRT to fund trainees for 12 months.

In addition to the 14 funded trainees, this grant with serve another 20 JHU PhD students who are 
not funded.  It will provide coursework, seminars, and advising for 60 JHU Masters and undergraduate
students and 40 Morgan State students, undergraduates and graduates combined.  These estimates
are based on an average flipped class size of 15 students and 20 active faculty. 
We expect to influence another 100 students who sample parts of the courseflow.
 

\subsubsection{A Modular, Customizable Data Science Curriculum}

We have seen great demand for data science at JHU.
In five years, more than 15 PhD students, many from physics and bio-medical engineering, have 
completed a 10 course CS Masters degree concurrently with their PhD program.  They typically
focus on databases, parallel programming, and algorithms. 

However, the current curriculum options serve very few students; most
are thwarted by institutional obstacles.  They only have time for a few extra-departmental 
courses.  Advisers and 
sponsored research push students to focus narrowly on projects and 
dissertations.  Masters students spend only three or four semesters 
and do not have spare time or resources to pursue data science.
Finally, the necessary content is diffuse across many courses,
which frustrates students that want to target specific skills.
Each course is a semester long and was designed for majors.
Finally, some of the necessary material is offered only at the undergraduate 
level.
An anecdote punctuates the problem.  In 2013, an incoming mechanical engineering graduate
student at JHU wanted to become a data scientist, i.e.~$\Pi$-shaped.  
We sat down to design a suitable curriculum.  We identified a 
{\em minimum} of 4 applied math courses and 6 computer science courses.

Examining the course content revealed that much of the material was redundant or 
irrelevant.  It is common practice in many computer science courses
to review undergraduate-level statistics.  Similarly,
applied math courses need to include some computing to support projects.  Also, courses are designed
for coverage of a topic area, not the development of skills.  For example, a 
computer architecture course covers topics needed for big data, such as the memory hierarchy
and Amdahl's law, but also includes many topics far too specific for a data scientist,
such as instruction set design, digital logic, and assembly language.

We have designed a modular course architecture in computing and applied math that
removes obstacles by eliminating redundancy and allowing the graduate student to 
pick and choose from a menu of topics to build the specific skills needed in 
a minimum amount of time.  The building block for this curriculum is a three week,
one unit focused topic course.  A PhD trainee will assemble the equivalent of 
three full courses by choosing nine topic courses.  Within this framework, the student
will have full autonomy to select courses, subject to prerequisites.
 We have worked with curriculum committees,
deans, department chairs, and the registrar to verify that this model will give proper credit
both to students toward graduate degrees and tenure-track faculty toward teaching obligations.  

We will teach flipped courses:
lectures and reading materials will be available online and 4-6 hours of classroom engagement per week
will focus on conducting projects and assignments.  This will reinforce 
skill acquisition for data scientists.  We have already taught several successful 
courses in this format, although they have been longer (3 unit courses) that aggregate multiple
data science topics.  PI Burns has been teaching {\em Data-Intensive Computing} (EN 600.423)  in the 
computer science department that covers cloud computing, parallel data-processing, and No-SQL 
databases.  Co-PI Budavari has been teaching {\em Practical Scientific Analysis of Big Data} 
(AS 171.628) through applied math (AMS) and {\em Graphics Processor Programming in CUDA} (AS 171.633) 
offered through physics.

Each topic course would be offered once per year in an order that a Master's student could 
complete a sequence of six topic courses in two semesters in their first year. 
PhD trainees will complete the nine courses in the first two years of their degree, 
coinciding with support from the NRT grant.

Most instructors will teach four topic courses per year, the equivalent of one 
four-unit semester course.  Instructors that teach fewer topic courses can amortize the teaching
credit over multiple years.

\begin{figure}
\begin{center}
\includegraphics[width=1.0\textwidth]{figs/cscurric.pdf}

\vspace{-10pt}

\caption{\small Data-science topic courses offered through the computer science 
department.  Arrows indicate prerequisites.    Two possible tracks for the 
{\em data wrangler} and {\em computational guru} are indicated in red and blue respectively
with common courses in purple.  Each course is annotated with some of the core 
concepts to be covered.}

\vspace{-15pt}

\label{fig:cscurric}
\end{center}
\end{figure}

\para{Course Flow in Computing}
%
We have designed a curriculum in computing for $\Pi$-shaped scientists and engineers
and identified examples of how one would assemble topic courses toward a specific
education goal.  Figure \ref{fig:cscurric} shows the one-unit topic courses that we
will offer initially, indicating prerequisite dependencies among courses.  We designed
the curriculum to be flexible toward multiple training outcomes, two of which we illustrate.   
An incoming fluid mechanics student that needs to develop new methods of 
building coupled, multi-scale simulations might wish to become a {\em computational guru}.
In addition to understanding the physics and mathematics of turbulence, the student
would be able to understand the algorithmic efficiency of computations and how to 
map them to modern many-core compute architectures and accelerators, GPUs and Xeon Phi.  
In contrast a student working on 
functional genomics that wanted to build a capability to compare gene expression across
species would train to be a {\em data wrangler}.  They might build a scale-out NoSQL 
database on the cloud and create a data-parallel correlation analysis using Spark---a distributed
in-memory parallel processing framework.
%RBTODO -- Spark in figure.


\begin{figure}
\begin{center}
\includegraphics[width=1.0\textwidth]{figs/amscurric.pdf}

\vspace{-10pt}

\caption{\small Data-science topic courses offered through applied mathematics and statistics. Arrows indicate prerequisites. Two possible tracks for the {\em statistics wizard} and {\em master of numerical analysis} are indicated in red and blue respectively with common courses in purple. Each course is annotated with some of the core concepts to be covered.} 
\label{fig:amscurric}

\vspace{-20pt}

\end{center}
\end{figure}

\para{Course Flow in Applied Mathematics and Statistics (AMS)}
%
The curricular model in applied mathematics follows that for computing
(Figure \ref{fig:amscurric}). 
Also shown are two possible training outcomes.
An astrophysics graduate student studying 
turbulent magnetic reconnection using high-resolution MHD data will
need to trace magnetic field-lines off the computational grid, 
evaluate combinations of spatial-derivatives of stored magnetic fields, 
and plot their isosurfaces. They will need to have command of the physics of MHD turbulence,
 reconnection, and plasma astrophysics and also must be a {\em numerical analysis maven}, 
able to develop interpolations with different smoothness properties and evaluate the 
accuracy of high-order derivative approximations.  On the other hand,  
a neuroscience student may wish to exploit high-resolution brain imaging 
and model neuronal connectivity. As a {\em statistics wizard}, they will be equipped to build 
a random graph model of brain connections and exploit it to make statistical 
inferences about neurophysiological processes.    

These will also be designed as flipped courses, with lectures and reading available online and classroom time devoted to practical analytic experience. Written homework will be assigned on the mathematical reading, which will be due on the day that material is used in class. Emphasis will be placed on developing computationally efficient solutions to the mathematical problems and on realistically assessing the errors of the models and approximations. 
%Office hours will be allotted to assisting students from other fields with technical mathematical material.  
The Core Participants in AMS have extensive experience in teaching  and mentoring students from other disciplines. 
Eyink taught one of the key courses a previous NSF training grant---an IGERT on Modeling Complex Systems.
The course, Advanced Parameterization in Science and Engineering (EN 550.695),  focused on developing interdisciplinary understanding among 
% attracting students from 
a broad array of backgrounds and scientific specialties. 
It continues to thrive after the expiration of the IGERT grant.


%\subsubsection{Interdisciplinary Advising}
%\label{sec:teams}
%
%
%Fundamental to realizing a $\Pi$-shaped trainee is engagement in multiple interdisciplinary 
%communities.  The concept is to pair two or three students within 
%the context of a specific research project with two or more advisers.  This core team 
%meets weekly and also works informally on project tasks daily.  At the next level 
%is the extended community that works ``in the area'' within a common science 
%domain.  This group also meets regularly under the 
%aegis of one of the Major Research Efforts.  At the highest level, the 
%IDIES institute connects multiple disciplines, making
%connections across science domains.  This community is glued together by the NRT Colloquia and
%the ``Data Science'' course.
%
%These communities replicate the success we have had with the Turbulence 
%Database group (Section \ref{sec:mre}).  Figure \ref{fig:tdg} shows the current community structure
%seen from the perspective of two core students in computer science and mechanical engineering.  This team
%has been remarkably successful at interdisciplinary work, e.g. with advances in computing enabling 
%a Nature paper \cite{Eyinketal13} and fluids computations forming the basis for a Supercomputing paper \cite{Kanov11}.
%In this NRT, we will build the infrastructure to support this hierarchy at the group and institute level,
%enhancing community with colloquia and courses.  
%
%
%\begin{wrapfigure}{r}{0.3\textwidth}
%\begin{centering}
%
%\vspace{-10pt}
%
%	\includegraphics[width=0.3\textwidth]{figs/grouphier.pdf}
%
%\vspace{-10pt}
%
%	\caption{\small Community hierarchy of a co-advised team.}
%
%\vspace{-15pt}
%
% \label{fig:tdg}
%\end{centering}
%\end{wrapfigure}
%
%Co-advising is formal; the JHU registrar
%will link multiple advisers 
%to a student and require advising meetings with both to approve 
%approve coursework and release registration holds.
%Masters students will realize the same benefit from the formation of study groups.  When possible, we will
%embed Masters students into extant research groups.  In other cases, we will form study groups 
%of four to six students with a major research effort with two advisers that will progress through 
%the program as a team.   While it is not mandatory to have a research component in a Masters degree,
%this NRT will be structured to connect Masters students to research.


\subsubsection{Data-Intensive Science Colloquium}

The tradition of interdisciplinary colloquia is well established at JHU.
For example, the Center of Environmental and Applied Fluid Mechanics weekly seminar 
is attended by faculty from six departments across
Engineering and Arts and Sciences, as well as members of the 
Applied Physics Laboratory (APL).  With the establishment of the NRT, a university-wide 
bi-weekly colloquium for data-intensive research will be formalized and will be 
perpetuated after the conclusion of the NRT under the umbrella of the Institute of 
Data Intensive Engineering and Science (IDIES).  The colloquium will include seminars by external,
guest speakers as well as internal speakers. Internal speakers will expose the NRT participants  
(including participants from UMD and Morgan) to the wide-range of data-intensive activities at JHU,
and the external speakers will expose them to the wider community, including opportunities
in the exascale workforce.  

In addition, two NRT trainees will present their work on the same day in a student-led event.
The faculty and the external guest speaker will attend the trainee colloquium, engage with the 
students and provide feedback on their work. The interaction will also provide an opportunity 
for the trainees to establish external contacts and gain visibility. 

A research showcase will be organized annually in which the trainees will present the outcome 
of their research and internships. The event will include posters that highlight the 
work of the trainees and presentations by the trainees themselves.  The showcases will also 
feature a guest speaker and will be attended by all the current and former trainees, 
participants from partner institutions, the advisory board members, industrial partners 
and potential employers.


\subsubsection{Career Development}
\label{sec:career}

Summer internships are a cornerstone in our plan to build an exascale workforce. 
We will build a formal network for student and trainee internships across the DOE National Labs, 
DOD laboratories (FFRDCs and UARCs), and industry.  We will provide two seminars yearly, open to students
and partners (Morgan State and UMD), to help students to apply and interview for internships in the fall and 
prepare them to enter the workforce in the spring.  The grant has partnered with Jim Ahrens as an
expert adviser to help design and run seminars, build the internship network,
and ensure that coursework prepares students.  Jim is uniquely qualified having run a student internship 
program at Los Alamos National Labs (LANL) for over a decade and having served on the 
DOE's Exascale Planning committee for five years.   He will work on this grant through his role as an adjunct research scientist at Johns Hopkins.

Increasingly, particularly in computer science, we see that summer internships serve as de facto
job interviews and play a crucial role in career planning for students.  Both undergraduate and graduate
students will spend multiple summers in different companies to learn about work culture, geography,
and career goals.  Then, they select a job often from one of their internships or, at a minimum, 
in a related opportunity informed by their intern experiences. This has become a remarkably successful
recruiting strategy for major tech companies, such as Facebook and Google.  We intend to replicate this
success for the exascale workforce.  We will work most closely with the DOE Labs and with the JHU Applied
Physics Laboratory---a DOD-funded lab with which JHU has a formal summer internship program.
However, our larger goals include engaging JHU's industry partners, such as Nvidia, Intel, and Microsoft, as well
as other supercomputing centers, NIH, ARL, NSF XD, etc.

This grant will leverage the shortage in well-trained data scientists \cite{mckinsey}
to secure internships for trainees.
%the budget does not provide funds for summer interns. 
Organizations planning for exascale 
are actively building groups that bridge the gap between science and computing, 
such as the Application Engineering group at Argonne National Labs and the 
Computational Science department at LANL.  These groups desperately need scientists 
that are both deeply committed to a science problem, such as climate change, and capable
of translating science problems to exascale computing.  Further, we feel that interns selected
through open competition are more likely to convert an internship into a job,
because the sponsor lab/company has invested in the intern's success.

In preparation for internships, the NRT-DESE will hold two day-long seminars available 
to trainees, NRT students, and partners (Morgan State and UMD).
The seminars will bring at least one senior member from the DOE,
from the DoD labs, and from industry; all will have
experience running summer internship programs. The first will occur in the fall and will 
help students apply to internships and prepare to interview.  We will provide 
briefings on different types of internships: the work, culture, and location.  
We will have a CV/resume writing workshop: the workshop will integrate feedback from the 
JHU Career Center and from hiring managers. We will also provide mock interviews.  
The second seminar will occur in the spring and will help prepare the student to 
enter the workforce.  This seminar will discuss general structure and expectations 
of employers in a summer job. It will emphasize how to be self motivated and self 
sufficient; key traits that lead to successful summer student positions.  
Sessions will focus on how to produce well-tested requirements-driven working 
software and how to successfully write a research paper with crisply defined 
research goals and contributions. These sessions will be supported with lessons 
from real-world industry and laboratory experiences. Future interns will also 
give a sample talk, briefly describing their research and skills, in preparation 
for presentations to their summer research groups.

%\rb{CFP: Projects must articulate explicit approaches to provide trainees with training and vocational counseling for both research and research-related careers, within and outside academia; preparation and structured used of individual development plans for trainees is highly recommended. Projects must provide explicit, formal training in technical skills, communication skills, and other transferable professional skills (e.g., project management, leadership, ethics, teaching, entrepreneurship, teamwork, conflict resolution, mentorship, and outreach).}

%\rb{CFP:  If internships are included, proposers should describe pre-internship orientation for trainees and hosts, duration, and expected outcomes. The proposed NRT should foster development of a global perspective, through experiences abroad and/or activities at the home institution(s).}


\subsubsection{Communication}
\label{sec:comm}

Each trainee will maintain an ePortfolio which will be updated at least every semester
and will be hosted on the NRT webpage.  
Each ePortfolio will list the trainee's background, modules, project activity, 
research interests, seminars, papers, etc. and external links of interest.  
These portfolios will fulfill a number of objectives:
(i) maintaining an online professional profile is an important training and 
will encourage the students to communicate their interest, experiences and accomplishments; 
(ii) the portfolio will be an asset while seeking internships and when joining the workforce; 
(iii) the portfolios of the cohort will foster the sense of a group; 
and, (iv) the data will be beneficial in the assessment of the impact of the 
NRT on the trainees' development (Section \ref{sec:eval}).  

To better equip all data science students with professional communication skills needed in academia, government or industry, we will require trainees to take three credits of Graduate Professional Communication Courses offered through the JHU Center for Leadership Education (CLE, \url{eng.jhu.edu/wse/cle}).
Like the courses developed through this NRT proposal, CLE courses are offered in a flexible modular format with a range of topics.
Some of this material was developed as a collaboration between CLE and our previous IGERT grant and is now enriching the entire graduate student community. We will enhance the CLE curriculum by developing units on the visual explanation of big data, including summarization and graphical statistical inference.
CLE modules will help students build the advanced communication skills critical for leveraging their academic experience in industry. They emphasize reporting information, polishing CVs and resumes, presenting conference papers, participating in poster sessions, tailoring information to both specialist and non-specialist audiences, 
entrepreneurship, and writing grant proposals for funding. The courses are designed specifically to help scientists and engineers improve their oral and written presentation skills in a practice-intensive environment.  Students will learn how to hone their message, to craft presentations that address both technical and non-technical audiences. All presentations will be recorded for self-evaluation, and students will receive extensive instructor and peer feedback.


\subsubsection{Sustainability and Scalability}

The project team has already overcome many of the obstacles to sustainability, particularly 
those around institutional support of non-traditional 1-unit topic courses. 
We have identified tenure-track faculty to 
teach and arranged for them to receive teaching credit. 
We have worked with the registrar to ensure that students can assemble 1-unit topics into
three credit courses that fulfill degree requirements.  
% We have worked with curriculum committees to pre-approve the course structure.

A next step toward sustainability lies in creating a culture in which the faculty are incented
and rewarded for interdisciplinary science and teaching.  If this is the case, the structure of the courses
and the community built within the NRT will perpetuate.  JHU, in general, and IDIES, in particular, 
are uniquely positioned.  In 2014, JHU 
 committed to 50 interdisciplinary faculty in its Bloomberg
Distinguished Professors.  Bloomberg Professors must be appointed and {\em teach} in two 
different {\em schools} in the University.  Two of the first twenty Bloomberg Professors came
from the IDIES core faculty.  IDIES also has a decade of experience with faculty working 
across disciplinary boundaries and receiving tenure.  
%Notably PI Burns closest and most
%ustained collaborators in his 15 years at JHU have been Meneveau (ME) and Szalay (Physics and Astronomy).

Our open-source approach to flipped courses will aid sustainability.  The project will build
a managed repository of coursework (with provenance) that can be implemented at any institution.
The video library of lectures supports these courses.  Instructors may run one of our flipped
courses based on online lectures and customize example projects to customize the classroom to 
the interests and projects of their learners. 

% RBTODO check this based on Alexander's letter
We have already started to explore implementing the NRT curriculum outside of an academic
setting.  There is interest in using coursework in a professional training setting 
and we have started discussions with the JHU Engineering Professionals
(EP) program to incorporate courses into continuing education.


%\rb{Rewrite.
%The key challenge to building a sustainable program lies in developing the curricula and the culture.
%We will use NRT funds and the five year period to build a community that teaches across disciplinary 
%%boundaries and focuses on skill development.  We feel that we have the 
%%foundation for such a community in the interdisciplinary research conducted by the 150 IDIES faculty.
%%We know that we have demand for such courses;
%many students already reach across departmental lines for coursework.}
%
%\rb{Not this.  Rewrite.  The flipped course format poses the key challenge to sustainability.  Topic courses
%will be offered with online lectures and hands-on, experiential work during course time.   This is 
%a particularly alternative approach for applied math, but is necessary to attack
%practical problems of personal interest.  The format taxes teaching assistants, 
%classroom infrastructure, and faculty time when compared with lectures.
%JHU has the infrastructure to teach flipped courses at scale; we have both 
%collaborative classrooms designed for small teams and laboratory classrooms with workstations.  
%In the NRT, we will create the culture of teaching more intensively.  The benefit to faculty is
%a tighter integration of teaching and research.  Our approach is not the most scalable, but
%we feel strongly that it is necessary to build $\Pi$-shaped scientists.}
